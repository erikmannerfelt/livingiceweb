#!/usr/bin/env bash

npm_packages=("photo-sphere-viewer" "leaflet" "leaflet.markercluster" "juxtaposejs")
run_npm=0

for package in  ${npm_packages[@]}; do
  if ! [[ -d static/plugins/node_modules/$package ]]; then
    run_npm=1
  fi
done

if [[ "$run_npm" == "1" ]]; then
  npm install --prefix ./static/plugins "${npm_packages[@]}"
fi
cargo build $@
