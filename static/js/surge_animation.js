const SURGE_ANIMATION_BASE_URL = "https://static.livingiceproject.com/surge_animations/";

/**
 * Fetch the surge animations geojson.
 *
 * @returns {Object}: The geojson with information about surge animations.
**/
async function get_surge_animations() {

	const data = await fetch(SURGE_ANIMATION_BASE_URL + "surge_animations.geojson").then(response => response.json());
	return data;
}

/**
 * Get the URL to a surge animation thumbnail.
 *
 * @param {String} key The key of the animation. 
 * @returns {String} The URL to the associated animation.
**/
function get_surge_anim_thumbnail(key) {
  return `${SURGE_ANIMATION_BASE_URL}/thumbnails/thumbnail_${key}.jpg`;
};

/**
 * Generate a table of information for the surge animation.
 *
 * @param {String} properties Properties of the surge animation. 
 * @returns {DOMPoint} A table of information.
**/
function surge_anim_description_table(properties) {
  	let description = document.createElement("table");
  	for (row of [
  			["Duration:", `${properties.start_date} to ${properties.end_date}`],
  			["Mode:", properties.mode],
  	]) {
  		let row_html = document.createElement("tr");
  		description.appendChild(row_html);

  		for (i in row) {
  			let kind = "td";
  			// The first entry is a "th" meaning it's formatted differently
  			if (i == 0) {
  				kind = "th";
  			};
  			let row_cell = document.createElement(kind);
  			row_html.appendChild(row_cell);
  			row_cell.textContent = row[i];
  		};
  	};
  	return description;
}

/**
 * Add content for the surge animation page.
 *
 * @param {String} key The key of the surge animation. 
**/
async function surge_anim_content(key) {

	const data = await get_surge_animations();

	// Try to find the properties for the key
	let properties;
	for (feature of data.features) {
	  if (feature.properties.key == key) {
	    properties = feature.properties;
	    break
	  };
	};

	// If that didn't work, show an error
	if (properties == undefined) {
	  let h1 = document.createElement("h1");
	  h1.textContent = `Server error: Animation '${key}' could not be found`;
	  document.body.appendChild(h1);
	  return;
	};

	let div = document.createElement("div");
	document.body.appendChild(div);
	div.style = "display: flex; flex-wrap: wrap; justify-content: center; align-items: center; flex-direction: column; margin-top: 17px;";
	
	let title = document.createElement("h1");
	div.appendChild(title);
	title.textContent = `Glacier surge: ${properties.name}`;

	div.appendChild(surge_anim_description_table(properties));

	let video_url = `https://static.livingiceproject.com/surge_animations/animations/${properties.animation_name}`;

	let link = document.createElement("a");
  link.href = video_url;
  link.text = "(direct link)"
  div.appendChild(link);

  let video = document.createElement("video");
  video.autoplay = true;
  video.loop = true;
  video.muted = true;
  video.inline = true;
  video.src = video_url;
  video.style = "max-height: 200%; max-width: 100%;";// padding: 2px; background-color: gray;";

  div.appendChild(video);
}

function setup_surge_animation (key) {

	const search_string = window.location.search.slice(1);
	const search_params = new URLSearchParams(search_string);

	if (search_params.has("ref")) {
	  const ref = search_params.get("ref").replace(/-AND-/gm, "&").slice(1, -1);
	  let back_button = document.getElementById("back_button");
	  back_button.href = back_button.href + "?" + ref;
	};

	surge_anim_content(key);
}
