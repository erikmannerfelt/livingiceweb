
// Nice hardcoded share symbol SVG!
const share_symbol = '<svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 30 30" width="30px" height="30px"><path d="M 23 3 A 4 4 0 0 0 19 7 A 4 4 0 0 0 19.09375 7.8359375 L 10.011719 12.376953 A 4 4 0 0 0 7 11 A 4 4 0 0 0 3 15 A 4 4 0 0 0 7 19 A 4 4 0 0 0 10.013672 17.625 L 19.089844 22.164062 A 4 4 0 0 0 19 23 A 4 4 0 0 0 23 27 A 4 4 0 0 0 27 23 A 4 4 0 0 0 23 19 A 4 4 0 0 0 19.986328 20.375 L 10.910156 15.835938 A 4 4 0 0 0 11 15 A 4 4 0 0 0 10.90625 14.166016 L 19.988281 9.625 A 4 4 0 0 0 23 11 A 4 4 0 0 0 27 7 A 4 4 0 0 0 23 3 z"/></svg>';

// Bindings are made in the global scope to allow the rel links to work properly.
let overlays;
let base_layers;
let map;

let current_search_string = "";

/**
 * Create a search string that can be shared with others
 *
 * @param {L.Map} map The map to generate a search string from
 * @param {{a: String, b: L.TileLayer}} base_layers The available base layers
 * @param {{a: String, b: L.FeatureGroup}} overlays The available overlays
 * @returns {String} The search string to share
**/
function create_search_string() {

	let search_params = new URLSearchParams("");
	
	for (const name in base_layers) {
		if (map.hasLayer(base_layers[name])) {
			search_params.set("base_layer", name);
			break;
		};
	};

	for (const name in overlays) {
		if (map.hasLayer(overlays[name])) {
			search_params.append("overlay", name);
		};
	};

	const center = map.getCenter();
	search_params.set("lat", center.lat);
	search_params.set("lon", center.lng);
	search_params.set("zoom", map.getZoom());

	return search_params.toString();
}

function modify_href_with_search_string(a_id) {
	let a = document.getElementById(a_id);

	a.href = a.href + "?ref=" + create_search_string();
	
}

/**
 * Get the longitude, latitude and zoom level from a search, or revert to default values
 *
 * @param {URLSearchParams} search_params The active search
 * @returns {[Number, Number, Number]} The lat, lon and zoom
**/
function get_lon_lat_zoom(search_params) {

	let lat = search_params.get("lat") || 70.0;
	let lon = search_params.get("lon") || 16.0;
	let zoom = search_params.get("zoom") || 4;

	return [lat, lon, zoom];
}

/**
 * Add the overlays that are given in the search
 *
 * @param {URLSearchParams} search_params The active search
 * @param {L.Map} map The active Leaflet map
 * @param {{a: String, b: L.FeatureGroup}} overlays The available overlays
**/
function add_overlays(search_params, map, overlays) {
	for (const name of search_params.getAll("overlay")) {
		if (name in overlays) {
			overlays[name].addTo(map);
		};
	};
}

/**
 * Add the base layer that is given in the search, or revert to a default one
 *
 * @param {URLSearchParams} search_params The active search
 * @param {L.Map} map The active Leaflet map
 * @param {{a: String, b: L.TileLayer}} base_layers The available base layers
**/
function add_base_layer(search_params, map, base_layers) {
	const base_layer = search_params.get("base_layer");
	if (base_layer in base_layers) {
		base_layers[base_layer].addTo(map);
	} else {
		base_layers["ESRI Satellite"].addTo(map);
	};
}

async function load_base_layers() {

	const layers = await fetch("/xyz_tiles.json").then(response => response.json());

	let base_layers = {}
	for (layer of layers) {
		base_layers[layer.title] = L.tileLayer(layer.url, {
			attribution: layer.attribution,
			minZoom: layer.minZoom,
			maxZoom: layer.maxZoom,
			tileSize: layer.tileSize,
			zoomOffset: layer.zoomOffset,
			bounds: (layer.bounds == null) ? [[-90, -180], [90, 180]] : layer.bounds,
			noWrap: true,
		});
	};

	return base_layers;
}

/** 
 * Set up the main map
**/
async function setup_map() {

	// Parse the search strings, e.g. "?base_layer=OpenStreetMap"
	const search_string = window.location.search.slice(1);
	const search_params = new URLSearchParams(search_string);

	// Parse the starting lat/lon/zoom from the search (or default to all of Svalbard)
	const [lat, lon, zoom] = get_lon_lat_zoom(search_params);

	map = L.map('map', {maxBounds: [[-120, -210], [120, 210]]}).setView([lat, lon], zoom);

	map.attributionControl.setPrefix("&copy; Erik Schytt Mannerfelt, <a href='https://uio.no'>UiO</a>, <a href='https://unis.no'>UNIS</a>");

	// Add generic functionality to add a text box anywhere
	L.Control.textbox = L.Control.extend({
		onAdd: function(map) {
			var text = L.DomUtil.create('div');
			text.style = this.options.style;
			text.id = this.options.id;
			text.innerHTML = this.options.text;//"<strong>Loading layers...</strong>";
			return text;
		},
		onRemove: function(map) {
			// Nothing to do here
		}
	});
	L.control.textbox = function(opts) { return new L.Control.textbox(opts);}

	L.Control.popuptext = L.Control.extend({
		onAdd: function(map) {

			let div = L.DomUtil.get("share-popup") || L.DomUtil.create('div');
			//let div = L.DomUtil.create('div');
			div.id = "share-popup";
			//div.class = "popup";
			div.onclick = function() {console.log("hello")};
			/*
			let inner = L.DomUtil.get("popuptext") || L.DomUtil.create('span', div);
			inner.id = "popuptext";
			inner.innerHTML = "Hello";
			*/
			div.innerHTML = '<div class="popup"><span class="popuptext" id="myPopup">Popup text...</span></div>';


			return div;
		},
		onRemove: function(map) {
			// Nothing to do here
		}
	});
	L.control.popuptext = function(opts) { return new L.Control.popuptext(opts);}

	// Add the loading text (which will later be removed)
	let loading_text = L.control.textbox({ text: "<strong>Loading layers...</strong>", id: "loading_text", style: "", position: 'topright' }).addTo(map);

	// Add the title in the bottom left corner
	L.control.textbox({text: "The Living Ice Project", id: "title", style: "font-family: arial; font-size: 1.5em; text-shadow: 1px 1px 5px white;", position: "bottomleft"}).addTo(map);

	base_layers = await load_base_layers();
	// Add the base layer set by the search or a default one.
	add_base_layer(search_params, map, base_layers);


	overlays = {

	};

	// Add a share button that will copy the current window to the clipboard
	let share_button = L.control.textbox({text: `<button style="border-radius: 100%; opacity: 0.8;">${share_symbol}</button>`, id: "share_button", style: "", position: "bottomright"}).addTo(map);

	share_button._container.onclick = function() {

		const url = window.location.origin + window.location.pathname + "?" + create_search_string();

		/* Copy the text inside the text field */
		navigator.clipboard.writeText(url);

		let popup = document.getElementById("share-popup") || document.createElement("div");
		// Remove all previous children (if any)
		while (popup.firstChild) {
			popup.removeChild(popup.firstChild);
		};
		popup.id = "share-popup";
		popup.style = "background-color: white; position: fixed; left: 50%; top: 50%; transform: translate(-60%, -50%); padding: 8px; z-index: 50000; border: 2px solid black; border-radius: 2%; max-width: 80%;";

		// Add a close button
		let popup_close = document.createElement("button");
		popup_close.id = "share-popup-close";
		popup.appendChild(popup_close);
		popup_close.innerHTML = "Close";
		popup_close.onclick = function() {popup.remove()};
		popup.appendChild(document.createElement("br"));

		// Add the link
		let popup_text = document.createElement("a");
		popup_text.id = "share-popup-text";
		popup_text.style = "font-size: 0.7em; color: black; word-break: break-all;";
		popup.appendChild(popup_text);
		popup_text.href = url;
		popup_text.innerHTML = url;

		document.body.appendChild(popup);
	};

	let styling = {
		collapsed: true,
	};
	

	// Finalize the map
	L.control.scale().addTo(map);
	let layer_control = L.control.layers(base_layers, overlays, styling).addTo(map);
	loading_text.remove();

	// These are asynchronous so they need to be awaited to load.

	let all_searched_overlays = search_params.getAll("overlay");


	// Asynchronously add all overlays. The "await Promise.all" awaits all overlays before continuing
	await Promise.all(
		// A nested list of [promise (function in progress), overlay name] pairs are mapped to be processed once they are ready
		[
			[add_surge_anims(), "Surge animations"],
			[add_spheres("/static/shapes/spheres.geojson"), "Spherical (360) images"],
			//[add_cryoclim(`/shapes/CryoClim_GAO_SJ_1990.geojson`, '#583470'), "Glaciers (1990)"],
			[add_rephotos("/static/shapes/rephotography.geojson"), "Retaken photographs"],
		].map(arr => {
			let [promise, name] = arr;

			// As soon as the promise is ready, process the associated layer
			promise.then(layer => {

				if (layer == null) {
					return;
				};

				// Add it to the layer dropdown
				layer_control.addOverlay(layer, name);

				// If it was searched for, show it on the map
				// Legacy "?layers=spheres" syntax. 
				const show_spheres = (name.includes("Spherical") & search_string.includes("spheres"));

				// Add the layer if it's included in the url query. If no queries are made, add the spherical image overlay.
				if (all_searched_overlays.includes(name) | show_spheres | ((all_searched_overlays.length == 0 & name == "Spherical (360) images"))) {
					layer.addTo(map);
				};
				// Save it in the overlays object to allow referencing later.
				overlays[name] = layer;
			});
		})
	);
}

/**
 * Interpolate between two hexadecimal numbers given a relative weight
 *
 * @param {String} hex0 The first hexadecimal number
 * @param {String} hex1 The second hexadecimal number
 * @param {Number} weight The weight of hex1 between 0 and 1
 * @returns {String} The interpolated hexadecimal number
**/
function interpolate_hex(hex0, hex1, weight) {

	let val0 = parseInt(hex0, 16);
	let val1 = parseInt(hex1, 16);

	return Math.round((val1 * weight + val0 * (1 - weight))).toString(16).padStart(2, "0");


}

/**
 * Naively interpolate between colors
 *
 * @param {{a: Number, b: String}} colors An object of value:hex_color items, e.g. {0: "#44ffcc"}
 * @param {Number} value The value to interpolate a color at
 * @returns {String} The interpolated color at `value`
**/
function interpolate_color(colors, value) {

	let lower = 0;
	let upper = 1;

	for (const xval in colors) {
		if (xval < value) {
			lower = xval;
		} else {

			upper = xval;
			break;
		};
	};

	const weight = (value - lower) / (upper - lower);


	let out_color = `#${interpolate_hex(colors[lower].slice(1, 3), colors[upper].slice(1, 3), weight)}${interpolate_hex(colors[lower].slice(3, 5), colors[upper].slice(3, 5), weight)}${interpolate_hex(colors[lower].slice(5, 7), colors[upper].slice(5, 7), weight)}`;

	return out_color;

}

/**
 * Add a spherical (360) images overview GeoJSON as a Leaflet FeatureGroup
 *
 * @param {String} url The url of the GeoJSON
 * @returns {L.FeatureGroup} The points representing spherical images
**/
async function add_spheres(url) {
	const data = await fetch(url).then(response => response.json());

	if (data.length == 0) {
		return;
	};

	const icon_svg = await fetch("/static/icons/spherical_image.svg").then(response => response.text());

	let colors = {
		1: "#505196",
		5: "#6bbdcf",
		6: "#6bcfa5",
		8: "#7ccf6b",
		9: "#cfb26b",
		11: "#946bcf",
		13: "#505196",
	};
	// Get an icon with a color that depends on its capture year
	function get_icon(properties) {
		// Convert the month to a color in hexadecimal
		const color = interpolate_color(colors, parseInt(properties.properties.date.slice(5, 7)) + parseInt(properties.properties.date.slice(8, 10)) / 30, 10).replace("#", "");

		return L.icon({
			iconUrl: `/icons/spherical_image.svg?color=${color}`,
			iconSize: [30, 70],
			iconAnchor: [15, 35],
			popupAnchor: [0, -20]
		});
	};

	let markers = L.markerClusterGroup({
		iconCreateFunction: function (cluster) {
			return L.divIcon({ 
				className: "dummyclassname",  // Not having this leaves a weird box
				iconAnchor: [30, 30],
				html: `<div style="width: 50px; position: absolute; top: 0%; left: 0%;">
				<img style="width: 100%; left: 0%; top: 0%; z-index: 0;" src=/icons/spherical_image.svg></img>
				<p style="position: absolute; left: 55%; top: 10%; text-shadow: 0px 0px 4px white; z-index: 1; font-size: 30px;">
					${cluster.getChildCount()}
				</p>
				</div>`
				},
			);
		},

	});
	let l = L.geoJSON(data, {
		pointToLayer: function(geoJsonPoint, latlng) {
			return L.marker(latlng, {icon: get_icon(geoJsonPoint)});

		}
	});
	markers.addLayer(l);
	markers.bindPopup(function (layer) {
		let thumbnail_path = `/static/${layer.feature.properties.thumbnail}`.replace("/static/static", "/static");
		let sphere_path = "/spheres/" + layer.feature.properties.filename.split("/").slice(-1)[0];//.replace("static", "");
		let current_search = create_search_string().replace(/&/gm, "-AND-");

		return `<a href='${sphere_path}?ref="${current_search}"' rel="noopener noreferrer">${layer.feature.properties.label}, date: ${layer.feature.properties.date}<img src="${thumbnail_path}" style="width: 100%"></img></a>`
	});

	return markers;


}

/**
 * Add the CryoCLIM shapefile GeoJSON as a Leaflet FeatureGroup
 *
 * @param {String} url The url of the GeoJSON
 * @param {String} color The color of the outlines
 * @returns {L.FeatureGroup} Polygons representing glaciers 
**/
async function add_cryoclim(url, color) {
	const data = await fetch(url).then(response => response.json());
	const markerHtmlStyles = `
		background-color: ${color};
		width: 1rem;
		height: 1rem;
		display: block;
		left: -0.5rem;
		top: -0.5rem;
		position: relative;
		border-radius: 1rem 1rem 0;
		transform: rotate(45deg);
		border: 1px solid #FFFFFF`;

	const icon = L.divIcon({
		className: `pin-${color}`,
		iconAnchor: [0, 0],
		labelAnchor: [-6, 0],
		popupAnchor: [0, -10],
		html: `<span style="${markerHtmlStyles}" />`
	});

	let l = L.geoJSON(data, {
		pointToLayer: function(geoJsonPoint, latlng) {
			return L.marker(latlng, {icon: icon});

		}
	}).bindPopup(function (layer) {return `Name: ${layer.feature.properties.NAME}, Area: ${(layer.feature.properties.Shape_Area / 1e6).toPrecision(2)} km², Length: ${(layer.feature.properties.LENGTH / 1e3).toPrecision(2)} km`});

	return l;
}

async function add_rephotos(url) {
	const color = "#ff0099";

	const data = await fetch(url).then(response => response.json());

	if (data.length == 0) {
		return;

	};

	const marker_icon = L.icon({
		iconUrl: "/static/icons/rephotography.svg",
		iconSize: [30, 70],
		iconAnchor: [22, 94],
		popupAnchor: [-3, -76],
	});
	
	let markers = L.markerClusterGroup({
		iconCreateFunction: function (cluster) {
			return L.divIcon({ 
				className: "dummyclassname",  // Not having this leaves a weird box
				iconAnchor: [30, 30],
				html: `<div style="width: 50px; position: absolute; top: 0%; left: 0%;">
				<img style="width: 100%; left: 0%; top: 0%; z-index: 0;" src=/static/icons/rephotography.svg></img>
				<p style="position: absolute; left: 60%; top: 30%; text-shadow: 0px 0px 4px white; z-index: 1; font-size: 30px;">
					${cluster.getChildCount()}
				</p>
				</div>`
				},
			);
		},


	});
	let l = L.geoJSON(data, {
		pointToLayer: function(_geoJsonPoint, latlng) {
			return L.marker(latlng, {icon: marker_icon});

		}
	});
	markers.addLayer(l);
	/*
	markers.bindPopup(function (layer) {
		let thumbnail_path = `/static/${layer.feature.properties.thumbnail}`.replace("/static/static", "/static");
		let sphere_path = "/spheres/" + layer.feature.properties.filename.split("/").slice(-1)[0];//.replace("static", "");

		console.log(sphere_path);
		return `<a href='${sphere_path}' target="_blank" rel="noopener noreferrer">${layer.feature.properties.label}, date: ${layer.feature.properties.date}<img src="${thumbnail_path}" style="width: 100%"></img></a>`
	});
	*/


	/*
	for (rephoto of data) {
		let marker = L.marker(
			[rephoto.latitude, rephoto.longitude],
			{
				"icon": marker_icon
			}
		);
		marker.rephoto = rephoto;

		markers.addLayer(marker);
	};
	*/

	markers.bindPopup(function (marker) {

		let link = document.createElement("a")

		link.text = `${marker.feature.properties.name} ${marker.feature.properties.before_year}-${marker.feature.properties.after_year}`

		//link.target = "_blank";
		link.rel = "noopener noreferrer";
		let search_string = create_search_string().replace(/&/gm, "-AND-");
		link.href = `/rephotography/${marker.feature.properties.rephoto_id}?ref="${search_string}"`
		return link;

		// return (
		// 	`<a href='/rephotography/${marker.feature.properties.rephoto_id}?ref="${compressed}"' target="_blank" rel="noopener noreferrer">${marker.feature.properties.name} ${marker.feature.properties.before_year}-${marker.feature.properties.after_year}</a>`
		// );

	});



	return markers;

}

async function add_surge_anims() {
	const data = await get_surge_animations();
	if (data.length == 0) {
		return;
	};

	let layer = L.geoJSON(data, {
		style: {
			color: "#444444",
		},
	});

	layer.bindPopup(function (box) {
		let properties = box.feature.properties;

		let div = document.createElement("div");

		// Add the name of the glacier as a title
		let title = document.createElement("h1");
		title.textContent = properties.name;
		div.appendChild(title);

		div.appendChild(surge_anim_description_table(properties));

		// Add a link to the animation, which also contains the thumbnail
		let link = document.createElement("a");
		div.appendChild(link);
		let search_string = create_search_string().replace(/&/gm, "-AND-");
		link.href = `/surge_animations/${properties.key}?ref="${search_string}"`;
		link.rel = "noopener noreferrer";

		// The thumbnail is part of the link, so clicking it opens the animation.
		let thumbnail = document.createElement("img");
		link.appendChild(thumbnail);
		thumbnail.src = get_surge_anim_thumbnail(properties.key);
		thumbnail.style = "max-width: 100%;";
		
		return div;

	}, {maxHeight: "30%"});
	return layer;
}
