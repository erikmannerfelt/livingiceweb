async function show_sphere(filename) {

	let metadata = await load_metadata(filename);

	let artist = "Unspecified";
	if (metadata.properties.artist != null) {
		artist = metadata.properties.artist;
	};

	const _viewer = new PhotoSphereViewer.Viewer({
		container: document.querySelector('#viewer'),
		panorama: `${metadata.properties.filename}`,
		caption: `${metadata.properties.label}, ${metadata.properties.date}. Photographer: ${artist}`,
	});

	const search_string = window.location.search.slice(1);
	const search_params = new URLSearchParams(search_string);

	if (search_params.has("ref")) {
	  const ref = search_params.get("ref").replace(/-AND-/gm, "&").slice(1, -1);
	  let back_button = document.getElementById("back_button");
	  back_button.href = back_button.href + "?" + ref;
	};
	
}


async function load_metadata(filename) {
	const data = await fetch("/static/shapes/spheres.geojson").then(response => response.json());

	return data.features.filter(feature => feature.properties.filename.includes(filename))[0];
};
