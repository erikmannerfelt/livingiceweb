
// Add the images and make sure they are always resized to fit the canvas properly
function add_images() {

		let div = document.getElementById("before_after");
		
		// Add the slider. The data are assumed to be in the div attributes
		slider = new juxtapose.JXSlider(
			"#before_after",
			[
				{
					src: div.attributes.before_url.nodeValue,
					label: div.attributes.before_label.nodeValue,
					credit: div.attributes.before_credit.nodeValue
				},
				{
					src: div.attributes.after_url.nodeValue,
					label: div.attributes.after_label.nodeValue,
					credit: div.attributes.after_credit.nodeValue
				}
			],
			{
				animate: false,
				showLabels: true,
				showCredits: true,
				startingPosition: "50%",
				makeResponsive: false,
			}
		);

		// Initialize the original widths and heights
		// These are used to calculate the aspect ratio
		let initial_width = 0;
		let initial_height = 0;

		// Function to resize the sliders (images) so that they never go out of frame.
		function update_sliders() {
			window.juxtapose.sliders.forEach(function (slider) {
				let max_width = document.documentElement.scrollWidth;
				// The height we want is the max height, minus the "preamble stuff" height, minus a bit more to include the labels.
				let max_height = document.documentElement.scrollHeight - div.offsetTop - 20;

				// If it's the first time running, assign the initial height/width values
				if (initial_width == 0) {
					initial_width = Number(slider.wrapper.style.width.replace("px", ""));
					initial_height = Number(slider.wrapper.style.height.replace("px", ""));
				}

				let aspect = (initial_width / initial_height);

				let new_width = max_width;
				// If the height of the image would go out of frame, scale the width down.
				if ((max_width / aspect) > max_height) {
					new_width = Math.round(aspect * max_height);
				};
				// Assign the new width.
				slider.wrapper.style.width = `${new_width}px`;

				let left = 0;
				// If the width is lower than the max, center the image by moving it right.
				if (new_width < max_width) {
					left = Math.round((max_width / 2) - (new_width / 2));
				}
				slider.wrapper.style.left = `${left}px`;
				slider.setWrapperDimensions();
			});
		}

		// Run the slider update on every load and resize
		window.addEventListener("resize", update_sliders);
		window.addEventListener("load", update_sliders);
}

function setup () {

	const search_string = window.location.search.slice(1);
	const search_params = new URLSearchParams(search_string);

	if (search_params.has("ref")) {
	  const ref = search_params.get("ref").replace(/-AND-/gm, "&").slice(1, -1);
	  let back_button = document.getElementById("back_button");
	  back_button.href = back_button.href + "?" + ref;
	};

	add_images();
	
}


