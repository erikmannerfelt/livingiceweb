#!/usr/bin/env bash

set -e

./build.sh $@

export SERVER_URL="https://schyttholmlund.com/share/GlacioSvalbard/static/"


SERVER_URL=$SERVER_URL cargo run --bin spheres
SERVER_URL=$SERVER_URL cargo run --bin rephotos
cargo run --bin web $@
