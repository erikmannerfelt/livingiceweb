{ pkgs ? import <nixpkgs> {} }:
let
  package = import ./default.nix { inherit pkgs; };


in pkgs.mkShell {
  inputsFrom = [ package ];

  buildInputs = with pkgs; [
    cargo-tarpaulin # Get test coverage statistics
    rustfmt
    clippy
  ];
  LIBCLANG_PATH = package.LIBCLANG_PATH;
  PKG_CONFIG_PATH = package.PKG_CONFIG_PATH;
  BINDGEN_EXTRA_CLANG_ARGS = package.BINDGEN_EXTRA_CLANG_ARGS;
  NODE_MODULES_PATH = package.NODE_MODULES_PATH;

  shellHook = ''

    ln -sf $NODE_MODULES_PATH static/plugins/node_modules
    export SERVER_URL="https://schyttholmlund.com/share/GlacioSvalbard/static/"


  '';
}
