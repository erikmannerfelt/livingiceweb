{pkgs ? import <nixpkgs> {}, port ? "5010", static_url ? "https://static.livingiceproject.com/"}:

let
  livingice = import (./default.nix) {inherit pkgs;};

in pkgs.stdenv.mkDerivation rec {

  pname = "livingice-bin";
  name = pname;

  buildInputs = [livingice];
  nativeBuildInputs = [pkgs.makeWrapper];
  unpackPhase = ''true'';

  buildPhase = ''
    mkdir -p $out/bin/
    ls ${livingice}/bin/ | while read binary; do
      makeWrapper "${livingice}/bin/$binary" "$out/bin/livingice-$binary"\
       --set ROCKET_PORT ${port}\
       --set ROCKET_ADDRESS 0.0.0.0\
       --set SERVER_URL ${static_url} 
    done

  '';
}