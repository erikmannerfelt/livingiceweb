{pkgs ? import <nixpkgs> {}}:

let
  manifest = (pkgs.lib.importTOML ./../Cargo.toml).package;

  # node_packages = import ./nodejs {inherit pkgs; nodejs=pkgs.nodejs_20;};
  node_modules = import ./node_modules.nix {inherit pkgs;};
  

in pkgs.rustPlatform.buildRustPackage {
  pname = manifest.name;
  version = manifest.version;
  cargoLock.lockFile = ./../Cargo.lock;
  nativeBuildInputs = with pkgs; [
    cargo # Manage rust projects`
    rustc # Compile rust
    pkg-config # Needed to install gdal-sys
    cmake
    gnumake
    clang
    zlib
    nodePackages.npm
    glib   # Required for gexiv2
  ];
  buildInputs = with pkgs; [ proj proj.dev libclang gdal openssl openssl.dev nodejs_20 gexiv2.dev glib ] ++ [node_modules];

  # libz-sys fails when compiling, so testing is disabled for now
  doCheck = false;

  src = pkgs.lib.cleanSource ./..;

  # Environment variables that are needed to compile
  LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";
  PKG_CONFIG_PATH =
    "$PKG_CONFIG_PATH:${pkgs.gdal}/lib/pkgconfig/:${pkgs.proj.dev}/lib/pkgconfig:${pkgs.gexiv2.dev}/lib/pkgconfig:${pkgs.glib}/lib/pkgconfig";
  BINDGEN_EXTRA_CLANG_ARGS =
    "-I ${pkgs.proj.dev}/include -I ${pkgs.clang}/resource-root/include -I ${pkgs.gcc}/include";

  NODE_MODULES_PATH="${node_modules}";

  preBuild = ''
    mkdir -p $out/static/plugins
    for files in static xyz_tiles.json text; do
      cp -r "$src/$files" $out/
    done

    ln -s ${node_modules} $out/static/plugins/node_modules
    cp -r $src/templates $out/
  '';
}
