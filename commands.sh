float32_cos=' --co "COMPRESS=DEFLATE" --co "PREDICTOR=3" --co "ZLEVEL=12" --co "TILED=YES"'
gdal2tiles_params=' --xyz -z "2-13" --processes=10'

# The 1936 dhdt and ortho maps
gdal2tiles.py $gdal2tiles_params original/ortho_1936_all_Svalbard_20m.tif
gdal_calc.py -A ~/Downloads/dh_1936_2010_all_Svalbard_50m.tif --calc "A / (2010 - 1936)" --outfile=dhdt_1936_2010_50m.tif $float32_cos
# The rendered dhdt is created in QGIS
gdal2tiles.py $gdal2tiles_params original/dhdt_1936_2010_50m_rendered.tif

# The 1990 - 2018 dhdt map
gdalbuildvrt /tmp/hello.vrt /media/hdd/Erik/Data/ArcticDEM/Svalbard/ArcticDEM_Svalbard_20m_upscaled_v3.0_wmask.tif /media/hdd/Erik/Data/NPI/DEM/NP_S0_DTM20_199095_33/S0_DTM20_199095_33.tif -separate
gdal_calc.py -A /tmp/hello.vrt --A_band=2 -B /tmp/hello.vrt --B_band=1 --calc "(B - A) / (2018 - 1990)" --outfile=dhdt_1990_2018_20m.tif $float32_cos
# Fix nodata
gdal_calc.py -A dhdt_1990_2018_20m.tif --calc="numpy.where(A > 1000, 3.4028234663852886e+38, A)" --outfile=fixed.tif

# Here, the rendered version is created in QGIS

gdal2tiles.py --xyz -z "2-13" --processes=10 original/dhdt_1990_2018_20m_rendered.tif
