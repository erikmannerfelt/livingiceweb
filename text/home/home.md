# The Living Ice Project
Welcome to the Living Ice Project, your gateway to explore glaciers around the world now and in the frame of change.

Find out about glaciers by accessing our interactive [**map**](/map).
You can switch between the project layers, GlacioGlobal and Glacier Rephotography in the upper right corner of the [map](/map). 

## Our mission
The Living Ice Project is dedicated to understanding and documenting the changes in glaciers across the globe in response to climate change. Through innovative projects like GlacioGlobal and Glacier Rephotography, we combine field data collection with public engagement, leveraging understanding of glaciers for everyone. 
Our mission is to bridge knowledge gaps, show and document glaciers and inspire global action by providing free, immersive access to the data collected through public engagement.


## Projects
The Living Ice Project consists of multiple sub-projects.
For more information, please see [our project page](/projects)
