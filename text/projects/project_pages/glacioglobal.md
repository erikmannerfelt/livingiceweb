## GlacioGlobal
- See more information on [Hedda Andersen's website](https://www.heddaandersen.com/blank-3).
- [See the spherical (360) image layer on the map](/map?overlay=Spherical+%28360%29+images)

### Overarching aim of GlacioGlobal 

Obtaining data in the field for glacier change analysis over time in collaboration with the general public.

GlacioGlobal is aimed towards bridging a knowledge gap in regards to loss of glacier ice in the Arctic and Antarctic relative to global climate changes.

### What makes GlacioGlobal important?

Understanding how and how much glaciers change over time in response to global climate change is crucial to understand the consequences of the processes driving glacier mass loss - locally, regionally and globally.

### How does it work?

By accessing glaciers globally through various tourism platforms in polar areas, we can monitor and analyze glacier changes over time and simultaneously engage and educate polar visitors and a global population by giving free access to our immersive glacier imagery and data.

