## Glacier Rephotography
- [See the "Rephotography" layer on the map.](/map?overlay=Retaken+photographs)

Repeat photography or rephotography is an ethnographic process which relies on visual contrast from two or more images taken during different points in time.
Our work with rephotography has stemmed from a quantitative approach where we attempt to locate the exact position of an original photograph, and retake a photograph with a modern digital camera of the same landscape at a later time.
We use rephotography to depict changes in landscapes from both climate and cultural perspectives, where glaciers or cultural remains such as huts or infrastructure have been the central points of our images.
Rephotography is a process which can transcend language and engage one’s critical thinking about places illustrated.
When contextualized with a written account or oral history the rephotography images can be better understood than without.

Our rephotography primarily draws from photographs which have originally been taken on the Svalbard archipelago. We plan to expand to other regions such as Antarcica, Greenland, Iceland, and Arctic Canada soon.

