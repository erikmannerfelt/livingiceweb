## Hedda Andersen - Norway

**Glaciology and climate science outreach.**

### Links

- [Personal website](https://heddaandersen.com)

Since moving to Svalbard in the Arctic in 2018, she’s had an unbreakable passion for the frozen parts of our world - both as a researcher and a guide.
She studied Arctic Geology (BSc level) and Glaciology (MSc level) with the University of Oslo and University Centre in Svalbard, specializing in remote sensing and spatial analysis.

Alongside her studies, she has also worked as an expedition guide at sea in the Arctic and Antarctic, and as a snowmobile guide locally on Svalbard.

Today she specializes in climate science communication and works on a daily basis with science outreach, alongside working as a guide in polar regions.

