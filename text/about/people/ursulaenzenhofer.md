## Ursula Enzenhofer - Austria
**Glaciology**

### Links
- [ResearchGate profile](https://www.researchgate.net/profile/Ursula-Enzenhofer)

Ursula has always been captivated by the dynamics and mechanisms of snow and ice.
Initially drawn to skiing, her passion for snow evolved into a clear academic pursuit during her university years.
This path led her to Svalbard, where she immersed herself in the intriguing world of snow and ice. After completing numerous courses, she chose to write her master's thesis in Svalbard, which paved the way for her PhD in Glaciology at NTNU in Trondheim.
She is dedicated to study and teach about the glaciated areas of our world.
She also worked as Science Coordinator for Hurtigruten Expeditions and is extremely passionate about educating people about the Cryosphere. 

Currently, Ursula focuses on Glacier Hydrology and Glacier Lake Outburst Floods in Norway, Chile, and Pakistan.
Her research also encompasses studying the changes in the dynamic response of glaciers and their interconnected hydrological networks.

