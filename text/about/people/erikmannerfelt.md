## Erik Schytt Mannerfelt - Sweden
**Glaciology.**

### Links

- [ResearchGate profile](https://www.researchgate.net/profile/Erik-Mannerfelt)
- [ORCiD profile](https://orcid.org/0000-0002-9146-557X)
- [3D model portfolio](https://sketchfab.com/ErikSH)

Erik has grown up in the glaciological community, with a father and grandfather in the field. 
After numerous fieldwork seasons in Sweden, his affinity for Svalbard took over in 2016 when moving there for studies at the University Centre in Svalbard (UNIS).

His work surrounds glacial instability in a changing climate; simplistic models of glacier mass loss may not capture extreme sudden events due to excessive melt.
The primary field site for his current work is Svalbard, but the studies dynamics also apply to Greenland and Antarctica, and are clear milestones to expand onto.

Erik is part of a multitude of scientific outreach projects in Sweden and Norway.
