## Kristoffer Ronning - Canada
**Rephotography, history, and logistics**

Kris is passionate about communicating places. He has completed his master’s degree in "friluftsliv" (outdoor education), where his thesis implemented repeat photography as a useful tool to convey both storytelling and place through different landscapes on Svalbard in the high Arctic.
He has studied in the Arctic Nature Guiding program on Svalbard and has worked with rephotography since 2019.
Currently, Kris works in the polar regions on expedition cruise ships where he gives history lectures and works as a sea kayak guide. 
