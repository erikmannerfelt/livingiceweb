{
  description = "Living Ice Project website";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs}:

    let 
      forAllSystems = drv:
        nixpkgs.lib.genAttrs [
          "aarch64-darwin"
          "aarch64-linux"
          "x86_64-linux"
          "x86_64-darwin"
          "x86_64-windows"
        ] (system: drv nixpkgs.legacyPackages.${system});
    in { 

      packages = forAllSystems (pkgs: rec {
          web = import ./nix/default.nix { inherit pkgs; };
          deployed = import ./nix/deployed.nix {inherit pkgs; };
          default = web;
      });
    
      devShells = forAllSystems (pkgs: {
        default = import ./nix/shell.nix { inherit pkgs; };
      });
      apps = forAllSystems (pkgs: 
        builtins.listToAttrs (builtins.map (name: (
            {
              inherit name;
              value = {
                type= "app";
                program = "${self.packages.${pkgs.system}.deployed}/bin/livingice-${name}";
              };
            }
          )) ["web" "spheres" "rephotos"])
      );

      overlays.default = final: prev: {
        livingice = self.packages.${prev.system}.deployed;
      };

      };
}
