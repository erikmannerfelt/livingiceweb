use std::path::PathBuf;

use chrono::TimeZone;
use serde::Deserialize;
use serde_json::Value;

use crate::errors::FetchError;
use crate::images::SphericalImage;
use crate::rephotography::Rephoto;

#[derive(Deserialize, Debug)]
struct EntryInfo {
    name: String,
    #[serde(rename = "mtime")]
    modified_time_str: String,
    #[serde(default = "default_size")]
    size: usize,
    #[serde(rename = "type")]
    entry_type: String,
}

fn default_size() -> usize {
    0
}

impl EntryInfo {
    fn modified_time(&self) -> i64 {
        chrono::DateTime::parse_from_rfc2822(&self.modified_time_str)
            .unwrap()
            .timestamp_millis()
    }

    fn from_geojson_feature(feature: &serde_json::Value) -> Result<EntryInfo, String> {
        let name = feature
            .get("filename")
            .and_then(|v| v.as_str())
            .ok_or("No filename in property")?
            .to_string();

        let modified_time_str = chrono::Utc
            .timestamp_millis(
                feature
                    .get("modified_date")
                    .and_then(|v| v.as_i64())
                    .ok_or("No modified date in property")?,
            )
            .to_rfc2822();

        Ok(EntryInfo {
            name,
            modified_time_str,
            size: 0,
            entry_type: "file".to_string(),
        })
    }
}

pub async fn index_spheres(
    geojson_filepath: &PathBuf,
    static_dir: Option<&PathBuf>,
    server_url: &str,
) -> Result<Vec<SphericalImage>, FetchError> {
    let sphere_url = server_url.to_string() + "/spheres/";

    let file_list =
        serde_json::from_str::<Vec<EntryInfo>>(&reqwest::get(&sphere_url).await?.text().await?)
            .map_err(|e| {
                format!(
                    "Formatting error on received spherical image list (URL: {sphere_url}): {e:?}"
                )
            })?;

    let mut existing = match read_sphericals_from_geojson(&geojson_filepath) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Could not read existing sphere GeoJSON. Reindexing.\n{e:?}");
            Vec::new()
        }
    };
    let supported_formats = crate::utilities::supported_formats();

    let mut new = Vec::<SphericalImage>::new();
    for entry in file_list {
        if !supported_formats
            .iter()
            .any(|s| entry.name.rsplit_once(".").unwrap_or(("", "")).1 == s)
        {
            continue;
        }

        if existing.iter().any(|other| {
            (other.filepath.file_name().unwrap().to_str().unwrap() == entry.name)
                & (other.modified_date == entry.modified_time())
        }) {
            continue;
        }

        let url = (if server_url.ends_with("/") {
            server_url.to_string()
        } else {
            server_url.to_string() + "/"
        }) + "spheres/"
            + &entry.name;
        let mut image = SphericalImage::from_url(&url, static_dir).await?;
        image.modified_date = entry.modified_time();
        new.push(image);
    }

    new.append(&mut existing);

    crate::images::save_sphericals_to_geojson(&new, geojson_filepath, static_dir)?;

    Ok(new)
}

pub async fn index_rephotos(
    geojson_filepath: &PathBuf,
    server_url: &str,
) -> Result<Vec<Rephoto>, FetchError> {
    let base_url = server_url.to_string() + "/rephotographs/";

    let file_list =
        serde_json::from_str::<Vec<EntryInfo>>(&reqwest::get(&base_url).await?.text().await?)
            .map_err(|e| {
                format!("Formatting error on received static file list (URL: {base_url}): {e:?}")
            })?;

    let mut rephotos = Vec::<Rephoto>::new();
    for entry in file_list {
        let url = base_url.clone() + &entry.name;

        let mut json = download_json(&(url.clone() + "/meta.json"))
            .await?
            .as_object()
            .and_then(|v| Some(v.to_owned()))
            .ok_or(format!("Could not convert JSON to object"))?;

        match json.insert(
            "modified_date".to_string(),
            serde_json::json!(entry.modified_time()),
        ) {
            _ => (),
        }
        //.ok_or(format!("Could not parse modified date"))?;
        rephotos.push(Rephoto::from_metadata(entry.name, url.clone(), json)?);
    }

    save_json(
        geojson_filepath,
        &crate::rephotography::rephotos_to_geojson(&rephotos),
    )?;

    Ok(rephotos)
}

fn save_json(filepath: &PathBuf, json_value: &Value) -> Result<(), FetchError> {
    if let Some(parent) = filepath.parent() {
        if !parent.is_dir() {
            std::fs::create_dir(parent)?;
        };
    }

    std::fs::write(
        filepath,
        serde_json::to_string_pretty(&json_value)
            .map_err(|e| format!("Error in converting sphericals to JSON: {e:?}"))?,
    )?;

    Ok(())
}

pub fn read_json(filepath: &PathBuf) -> Result<Value, FetchError> {
    Ok(serde_json::from_str(&std::fs::read_to_string(filepath)?)
        .map_err(|e| format!("Could not read JSON: {e}"))?)
}

fn read_sphericals_from_geojson(filepath: &PathBuf) -> Result<Vec<SphericalImage>, String> {
    let data = serde_json::from_str::<serde_json::Value>(
        &std::fs::read_to_string(filepath)
            .map_err(|e| format!("Could not read spherical geojson: {e:?}"))?,
    )
    .map_err(|e| format!("Could not convert spherical geojson to json: {e:?}"))?;

    let features = data
        .get("features")
        .ok_or("No features found")?
        .as_array()
        .ok_or("Features is not an array")?;

    let mut entry_infos = Vec::<SphericalImage>::new();
    for feature in features {
        let entry_info = SphericalImage::from_geojson_feature(feature).unwrap();

        entry_infos.push(entry_info);
    }

    Ok(entry_infos)
}

pub async fn download_json(url: &str) -> Result<Value, FetchError> {
    let response = reqwest::get(url).await?.error_for_status()?;

    Ok(serde_json::from_slice(&response.bytes().await?)
        .map_err(|e| format!("Could not parse JSON: {e}"))?)
}

pub async fn download(url: &str, filepath: &PathBuf) -> Result<(), FetchError> {
    println!("Downloading URL: {url}");
    let response = reqwest::get(url).await?.error_for_status()?;

    std::fs::write(filepath, response.bytes().await?)?;

    Ok(())
}

pub async fn get_metadata() {}
