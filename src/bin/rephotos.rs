fn main() {
    let config = livingiceweb::interface::ServerConfig::from_env();
    let geojson_filepath = config.static_dir.join("shapes/rephotography.geojson");

    let _ = tokio::runtime::Runtime::new()
        .unwrap()
        .block_on(livingiceweb::io::index_rephotos(
            &geojson_filepath,
            &config.server_url,
        ))
        .unwrap();
}
