use notify::{event::EventKind, Watcher};
use std::path::PathBuf;

fn handle_event(
    res: notify::Result<notify::Event>,
    text_dir: &PathBuf,
) -> Result<(), Box<dyn std::error::Error>> {
    let modified_files = match res {
        Ok(event) => match event.kind {
            EventKind::Modify(_) => Some(event.paths),
            _ => None,
        },
        Err(e) => {
            eprintln!("Error: {e:?}");
            None
        }
    };

    if let Some(modified_files) = modified_files {
        for file in modified_files {
            println!("(Re-)rendering {file:?}");
            livingiceweb::text::render_md(&file, text_dir)?;
        }
    }
    Ok(())
}
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let text_dir: PathBuf = PathBuf::from("text/");

    println!(
        "{:?}",
        livingiceweb::text::make_text_context(text_dir.clone())
    );
    return Ok(());
    println!("(Re-)rendering all files in {text_dir:?}");
    livingiceweb::text::render_all_md(&text_dir)?;

    let mut watcher = notify::recommended_watcher(|res| {
        handle_event(res, &PathBuf::from("text/")).unwrap();
    })?;

    println!("Watching {text_dir:?} for changes");
    watcher.watch(&text_dir, notify::RecursiveMode::Recursive)?;

    loop {}

    //Ok(())
}
