use std::path::Path;

fn main() {
    let config = livingiceweb::interface::ServerConfig::from_env();
    let geojson_filepath = config.static_dir.join("shapes/spheres.geojson");

    let _data = tokio::runtime::Runtime::new()
        .unwrap()
        .block_on(livingiceweb::io::index_spheres(
            &geojson_filepath,
            Some(&config.static_dir),
            &config.server_url,
        ))
        .unwrap();
}
