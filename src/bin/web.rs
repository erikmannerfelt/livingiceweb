#[macro_use]
extern crate rocket;
#[launch]
fn rocket() -> _ {
    livingiceweb::interface::build_rocket().unwrap()
}
