use cached::proc_macro::cached;
use serde_json::Value;
use std::{
    collections::HashMap,
    fs::FileType,
    path::{Path, PathBuf},
};

pub fn parse_md(filepath: &PathBuf) -> Result<String, std::io::Error> {
    Ok(markdown::to_html(&std::fs::read_to_string(filepath)?).to_owned())
}

pub fn render_md(filepath: &PathBuf, base_dir: &PathBuf) -> std::io::Result<()> {
    let html = parse_md(filepath)?;

    let out_dir = PathBuf::from("templates/text/");

    let filepath = filepath.canonicalize()?.to_owned();
    let base_path = filepath.strip_prefix(base_dir.canonicalize()?).unwrap();

    let out_path = out_dir.join(base_path.with_extension("html"));

    if let Some(parent) = out_path.parent() {
        if !parent.is_dir() {
            std::fs::create_dir_all(parent)?;
        }
    }

    std::fs::write(out_path, html)?;

    Ok(())
}

pub fn read_dir_rec(
    directory: &PathBuf,
    i: usize,
    max_rec: usize,
) -> std::io::Result<Vec<PathBuf>> {
    if i > max_rec {
        return Ok(Vec::new());
    }
    let mut files = Vec::<PathBuf>::new();

    for entry in directory
        .read_dir()?
        .filter_map(|e| e.ok().and_then(|e| Some(e.path())))
    {
        if entry.is_dir() {
            files.append(&mut read_dir_rec(&entry, i + 1, max_rec)?);
        } else if entry.is_file() {
            files.push(entry);
        }
    }

    Ok(files)
}

pub fn render_all_md(text_dir: &PathBuf) -> std::io::Result<()> {
    for filepath in read_dir_rec(text_dir, 0, 10)? {
        if filepath.extension().and_then(|s| s.to_str()) == Some("md") {
            render_md(&filepath, text_dir)?;
        }
    }
    Ok(())
}

fn rec_make_text_context(
    values: &[(&[String], &Value)],
    i: usize,
    max_rec: usize,
) -> serde_json::Map<String, serde_json::Value> {
    let mut out = serde_json::Map::<String, serde_json::Value>::new();

    if i > max_rec {
        return out;
    }

    //let mut base_removed = Vec::<(&String, &[String], &Value)>::new();
    let mut base_removed = HashMap::<String, Vec<(&[String], &Value)>>::new();

    for (parents, value) in values {
        if parents.len() == 1 {
            let value = value.clone();
            out.insert(parents[0].clone(), value.clone());
        } else if let Some((first_parent, rest)) = parents.split_first() {
            if !base_removed.contains_key(first_parent) {
                base_removed.insert(first_parent.clone(), Vec::new());
            };

            if let Some(inner) = base_removed.get_mut(first_parent) {
                inner.push((rest, value));
            }
        };
    }

    for (key, parts) in base_removed.iter() {
        out.insert(
            key.clone(),
            Value::Object(rec_make_text_context(&parts, i + 1, max_rec)),
        );
    }

    out
}

//#[cached]
pub fn make_text_context(
    text_dir: PathBuf,
) -> Result<serde_json::Map<String, serde_json::Value>, String> {
    let mut texts = Vec::<(Vec<String>, Value)>::new();
    let text_dir = text_dir.canonicalize().unwrap();
    let filepaths: Vec<PathBuf> = crate::text::read_dir_rec(&text_dir, 0, 10)
        .unwrap()
        .iter()
        .map(|p| p.to_owned())
        .collect();
    for filepath in filepaths {
        let md = crate::text::parse_md(&filepath).unwrap();

        let path = filepath.strip_prefix(&text_dir).unwrap();

        let mut parents = Vec::<String>::new();
        for part in path.parent().unwrap().components() {
            parents.push(part.as_os_str().to_str().unwrap().to_string());
        }

        parents.push(path.file_stem().unwrap().to_str().unwrap().to_string());

        texts.push((parents, serde_json::Value::String(md)));
    }

    let refs = texts
        .iter()
        .map(|(p, v)| (p.as_slice(), v))
        .collect::<Vec<(&[String], &Value)>>();

    Ok(rec_make_text_context(&refs, 0, 10))
}
