use std::{collections::HashMap, path::PathBuf};

use serde::Serialize;
use serde_json::{Map, Value};

use crate::errors::FetchError;

#[derive(Debug, Serialize)]
pub struct Rephoto {
    pub rephoto_id: String,
    pub name: String,
    pub longitude: f64,
    pub latitude: f64,

    pub before_year: u32,
    pub before_month: Option<u32>,
    pub before_day: Option<u32>,
    pub before_credit: String,
    pub before_filepath: PathBuf,

    pub after_year: u32,
    pub after_month: Option<u32>,
    pub after_day: Option<u32>,
    pub after_credit: String,
    pub after_filepath: PathBuf,

    pub modified_date: i64,

    pub extra_metadata: Map<String, Value>,
}

impl Rephoto {
    pub fn from_dir(dirname: &PathBuf) -> Result<Rephoto, Box<dyn std::error::Error>> {
        let mut metadata: Map<String, Value> =
            serde_json::from_str::<Value>(&std::fs::read_to_string(dirname.join("meta.json"))?)?
                .as_object()
                .unwrap()
                .clone();

        let mut parsed_strings = HashMap::<String, String>::new();
        for key in [
            "before_date",
            "after_date",
            "before_credit",
            "after_credit",
            "name",
        ] {
            parsed_strings.insert(
                key.to_string(),
                match metadata.remove(key) {
                    Some(o) => Ok(match o {
                        Value::Number(v) => Some(v.to_string()),
                        _ => o.as_str().and_then(|s| Some(s.to_string())),
                    }
                    .unwrap()),
                    None => Err(format!(
                        "Could not parse photograph {dirname:?}. Key {key} missing in meta.json"
                    )),
                }?,
            );
        }
        let mut parsed_numbers = HashMap::<String, f64>::new();

        for key in ["latitude", "longitude"] {
            parsed_numbers.insert(
                key.to_string(),
                metadata.remove(key).and_then(|o| o.as_f64()).unwrap(),
            );
        }

        let rephoto_id = dirname
            .file_stem()
            .and_then(|osstr| osstr.to_str())
            .and_then(|s| s.split("-").next())
            .unwrap()
            .to_string();

        let mut dates: [[Option<u32>; 3]; 2] = [[None; 3]; 2];

        for (i, key) in ["before_date", "after_date"].iter().enumerate() {
            for (j, date_str) in parsed_strings[*key].split("-").enumerate() {
                dates[i][j] = date_str.parse::<u32>().ok()
            }
        }

        Ok(Rephoto {
            rephoto_id,
            name: parsed_strings["name"].clone(),
            longitude: parsed_numbers["longitude"],
            latitude: parsed_numbers["latitude"],
            before_year: dates[0][0].unwrap(),
            before_month: dates[0][1],
            before_day: dates[0][2],
            before_credit: parsed_strings["before_credit"].clone(),
            before_filepath: dirname.join("before.jpg"),
            after_year: dates[1][0].unwrap(),
            after_month: dates[1][1],
            after_day: dates[1][2],
            after_credit: parsed_strings["after_credit"].clone(),
            after_filepath: dirname.join("after.jpg"),
            extra_metadata: metadata,
            modified_date: 0,
        })
    }

    pub fn from_geojson_file(filepath: &PathBuf, rephoto_id: &str) -> Result<Rephoto, FetchError> {
        let geojson_data = crate::io::read_json(filepath)?;

        let rephoto_json = geojson_data
            .get("features")
            .ok_or("Could not find features in rephoto geojson")?
            .as_array()
            .unwrap()
            .iter()
            .filter(|f| {
                f.get("properties")
                    .and_then(|v| v.get("rephoto_id"))
                    .and_then(|v| v.as_str())
                    .and_then(|s| Some(s == rephoto_id))
                    .unwrap_or(false)
            })
            .next()
            .ok_or(format!("Could not find photo with id: {rephoto_id}"))?;

        Ok(Rephoto::from_geojson_feature(&rephoto_json)?)
    }

    pub fn from_geojson_feature(geojson_feature: &Value) -> Result<Rephoto, FetchError> {
        let properties = geojson_feature
            .get("properties")
            .ok_or("No properties in feature")?;

        let rephoto_id = properties
            .get("rephoto_id")
            .and_then(|v| v.as_str())
            .ok_or("No rephoto_id in property")?
            .to_string();

        let name = properties
            .get("name")
            .and_then(|v| v.as_str())
            .ok_or("No name in property")?
            .to_string();

        let before_year = properties
            .get("before_year")
            .and_then(|v| v.as_i64())
            .and_then(|v| Some(v as u32))
            .ok_or("No before_year in property")?;
        let before_month: Option<u32> = properties
            .get("before_month")
            .and_then(|v| v.as_i64())
            .and_then(|v| Some(v as u32));
        let before_day: Option<u32> = properties
            .get("before_day")
            .and_then(|v| v.as_i64())
            .and_then(|v| Some(v as u32));

        let after_year = properties
            .get("after_year")
            .and_then(|v| v.as_i64())
            .and_then(|v| Some(v as u32))
            .ok_or("No after_year in property")?;
        let after_month: Option<u32> = properties
            .get("after_month")
            .and_then(|v| v.as_i64())
            .and_then(|v| Some(v as u32));
        let after_day: Option<u32> = properties
            .get("after_day")
            .and_then(|v| v.as_i64())
            .and_then(|v| Some(v as u32));

        let before_credit = properties
            .get("before_credit")
            .and_then(|v| v.as_str())
            .ok_or("No before_credit in property")?
            .to_string();
        let after_credit = properties
            .get("after_credit")
            .and_then(|v| v.as_str())
            .ok_or("No after_credit in property")?
            .to_string();

        let before_filepath = properties
            .get("before_filepath")
            .and_then(|v| v.as_str())
            .and_then(|s| Some(PathBuf::from(s)))
            .ok_or("No before_filepath in property")?;
        let after_filepath = properties
            .get("after_filepath")
            .and_then(|v| v.as_str())
            .and_then(|s| Some(PathBuf::from(s)))
            .ok_or("No after_filepath in property")?;

        let modified_date = properties
            .get("modified_date")
            .and_then(|v| v.as_i64())
            .ok_or("No modified_date in property")?;

        let coords = geojson_feature
            .get("geometry")
            .and_then(|v| v.get("coordinates"))
            .and_then(|v| v.as_array())
            .ok_or("No geometry in feature")?
            .iter()
            .filter_map(|v| v.as_f64())
            .collect::<Vec<f64>>();

        let longitude = *coords.get(0).ok_or("No longitude in coordinate")?;
        let latitude = *coords.get(1).ok_or("No latitude in coordinate")?;

        Ok(Rephoto {
            rephoto_id,
            name,
            longitude,
            latitude,
            before_year,
            before_month,
            before_day,
            before_credit,
            before_filepath,
            after_year,
            after_month,
            after_day,
            after_credit,
            after_filepath,
            modified_date,
            extra_metadata: serde_json::json!({}).as_object().unwrap().to_owned(),
        })
    }

    pub fn from_metadata(
        id: String,
        filepath_base: String,
        metadata: Map<String, Value>,
    ) -> Result<Rephoto, FetchError> {
        let mut metadata = metadata;
        // let mut metadata: Map<String, Value> =
        //     serde_json::from_str::<Value>(&std::fs::read_to_string(dirname.join("meta.json"))?)?
        //         .as_object()
        //         .unwrap()
        //         .clone();

        let mut parsed_strings = HashMap::<String, String>::new();
        for key in [
            "before_date",
            "after_date",
            "before_credit",
            "after_credit",
            "name",
        ] {
            parsed_strings.insert(
                key.to_string(),
                match metadata.remove(key) {
                    Some(o) => Ok(match o {
                        Value::Number(v) => Some(v.to_string()),
                        _ => o.as_str().and_then(|s| Some(s.to_string())),
                    }
                    .unwrap()),
                    None => Err(format!(
                        "Could not parse photograph {id:?}. Key {key} missing in meta.json"
                    )),
                }?,
            );
        }
        let mut parsed_numbers = HashMap::<String, f64>::new();

        for key in ["latitude", "longitude"] {
            parsed_numbers.insert(
                key.to_string(),
                metadata.remove(key).and_then(|o| o.as_f64()).unwrap(),
            );
        }

        let rephoto_id = id.split("-").next().unwrap().to_string();

        let mut dates: [[Option<u32>; 3]; 2] = [[None; 3]; 2];

        for (i, key) in ["before_date", "after_date"].iter().enumerate() {
            for (j, date_str) in parsed_strings[*key].split("-").enumerate() {
                dates[i][j] = date_str.parse::<u32>().ok()
            }
        }

        let modified_date = metadata
            .get("modified_date")
            .and_then(|v| v.as_i64())
            .unwrap_or(0);

        Ok(Rephoto {
            rephoto_id,
            name: parsed_strings["name"].clone(),
            longitude: parsed_numbers["longitude"],
            latitude: parsed_numbers["latitude"],
            before_year: dates[0][0].unwrap(),
            before_month: dates[0][1],
            before_day: dates[0][2],
            before_credit: parsed_strings["before_credit"].clone(),
            before_filepath: PathBuf::from(filepath_base.clone() + "/before.jpg"),
            after_year: dates[1][0].unwrap(),
            after_month: dates[1][1],
            after_day: dates[1][2],
            after_credit: parsed_strings["after_credit"].clone(),
            after_filepath: PathBuf::from(filepath_base + "/after.jpg"),
            extra_metadata: metadata,
            modified_date,
        })
    }

    pub fn before_date_str(&self) -> String {
        date_str(self.before_year, self.before_month, self.before_day)
    }

    pub fn after_date_str(&self) -> String {
        date_str(self.after_year, self.after_month, self.after_day)
    }
}

fn date_str(year: u32, month: Option<u32>, day: Option<u32>) -> String {
    if let Some(month_str) = month.and_then(|m| {
        [
            "Jan.", "Feb.", "Mar.", "Apr.", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec.",
        ]
        .get((m - 2) as usize)
    }) {
        let day_str = day
            .and_then(|d| Some(d.to_string() + " "))
            .unwrap_or("".to_string());
        format!("{day_str}{month_str} {year}")
    } else {
        year.to_string()
    }
}

pub fn find_rephotos(dirname: &PathBuf) -> Result<Vec<Rephoto>, Box<dyn std::error::Error>> {
    let mut rephotos = Vec::<Rephoto>::new();

    for dir in dirname.read_dir()?.filter_map(|d| d.ok()) {
        rephotos.push(Rephoto::from_dir(&dir.path())?);
    }

    Ok(rephotos)
}

pub fn rephotos_to_geojson(rephotos: &Vec<Rephoto>) -> Value {
    serde_json::json!({
        "type": "FeatureCollection",
        "crs": {
            "type": "name",
            "properties": {
                "name": "urn:ogc:def:crs:OGC:1.3:CRS84",
            }
        },
        "features": rephotos.iter().map(|r| serde_json::json!({
            "type": "Feature",
            "properties": {
                "rephoto_id": r.rephoto_id,
                "name": r.name,
                "before_year": r.before_year,
                "before_month": r.before_month,
                "before_day": r.before_day,
                "before_credit": r.before_credit,
                "before_filepath": r.before_filepath,
                "after_year": r.after_year,
                "after_month": r.after_month,
                "after_credit": r.after_credit,
                "after_filepath": r.after_filepath,
                "modified_date": r.modified_date,
            },
            "geometry": {
                "type": "Point",
                "coordinates": [r.longitude, r.latitude]
            }
        })).collect::<Vec<Value>>()
    })
}
