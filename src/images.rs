use std::path::{Path, PathBuf};

use crate::errors::FetchError;
use chrono::TimeZone;
use rexiv2::Metadata;
use serde_json::{json, Map, Value};

/*
pub fn read_image_metadata(filepath: &PathBuf) -> Metadata {

    let metadata = Metadata::new_from_path(&filepath).unwrap();
}
*/

#[derive(Debug)]
pub struct SphericalImage {
    pub filepath: PathBuf,
    pub sphere_id: String,
    pub date: String,
    pub description: String,
    pub artist: Option<String>,
    pub longitude: f64,
    pub latitude: f64,
    pub modified_date: i64,
}

impl SphericalImage {
    pub fn from_path(filepath: &PathBuf) -> Result<SphericalImage, FetchError> {
        let (sphere_id, date, description) = crate::utilities::match_spherical_name(filepath)?;

        let metadata = load_metadata(filepath);

        let (longitude, latitude, _) = match crate::utilities::coordinates_from_metadata(&metadata)
        {
            Some(v) => Ok(v),
            None => Err(format!("No coordinate metadata in file {filepath:?}")),
        }?;
        let artist = metadata
            .get(&"Exif.Image.Artist".to_string())
            .and_then(|o| o.as_array())
            .and_then(|a| a.get(0))
            .and_then(|o| o.as_str())
            .and_then(|s| Some(s.to_string()));

        let modified_date_str = metadata
            .get(&"Exif.Image.DateTime".to_string())
            .and_then(|o| o.as_array())
            .and_then(|a| a.get(0))
            .and_then(|v| v.as_str())
            .ok_or("No image DateTime".to_string())?;

        let modified_date = chrono::Utc
            .datetime_from_str(modified_date_str, "%Y:%m:%d %H:%M:%S")
            .map_err(|e| format!("Datetime pattern not matched: {e:?}"))?
            .timestamp_millis();

        Ok(SphericalImage {
            sphere_id,
            date,
            description,
            filepath: filepath.clone(),
            longitude,
            latitude,
            artist,
            modified_date,
        })
    }

    pub async fn from_url(
        url: &str,
        static_dir: Option<&PathBuf>,
    ) -> Result<SphericalImage, FetchError> {
        let url_path = Path::new(url);
        //let extension = ".".to_string() + url.rsplit_once(".").unwrap().1;

        //let temp_file = tempfile::Builder::new().prefix(url_path.file_stem().unwrap()).suffix(&extension).rand_bytes(0).tempfile()?;
        let temp_dir = tempfile::tempdir()?;

        let temp_path = temp_dir.path().to_path_buf().join(
            url_path
                .file_name()
                .ok_or("URL has no filename: {url}".to_string())?,
        );

        crate::io::download(url, &temp_path).await?;

        let mut image = SphericalImage::from_path(&temp_path)?;

        image.save_thumbnail(&image.default_thumbnail_path(static_dir)?)?;

        image.filepath = Path::new(url).to_path_buf();

        Ok(image)
    }

    pub fn default_thumbnail_path(
        &self,
        static_dir: Option<&PathBuf>,
    ) -> Result<PathBuf, FetchError> {
        let filename = self
            .filepath
            .with_extension("thumbnail")
            .file_name()
            .and_then(|s| s.to_str().and_then(|s2| Some(s2.to_string())))
            .ok_or(format!(
                "Could not convert filepath to a thumbnail path: {:?}",
                self.filepath
            ))?;

        let static_dir: PathBuf = static_dir
            .and_then(|d| Some(d.clone()))
            .unwrap_or(PathBuf::from("static"));

        Ok(static_dir.join(format!("thumbnails/{filename}")))

        //self.filepath.parent().unwrap().join(format!("thumbnails/{filename}"))
    }

    pub fn save_thumbnail(&self, filepath: &PathBuf) -> Result<(), FetchError> {
        let metadata = Metadata::new_from_path(&self.filepath)
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, format!("{e:?}")))?;
        if let Some(data) = metadata.get_preview_images().and_then(|prevs| {
            prevs
                .iter()
                .filter(|prev| prev.get_width() <= 1024_u32)
                .find_map(|prev| prev.get_data().ok())
        }) {
            let img = image::load_from_memory(&data)?;

            if let Some(parent) = filepath.parent() {
                if !parent.is_dir() {
                    std::fs::create_dir_all(parent)?;
                }
            }
            img.save_with_format(filepath, image::ImageFormat::Jpeg)?;

            return Ok(());
        };

        Err("Could not find a thumbnail".to_string().into())
    }

    pub fn from_geojson_feature(feature: &Value) -> Result<SphericalImage, String> {
        let properties = feature
            .get("properties")
            .ok_or("No properties in feature")?;

        let filename = properties
            .get("filename")
            .and_then(|v| v.as_str())
            .ok_or("No filename in property")?
            .to_string();

        let filepath = Path::new(&filename).to_path_buf();

        let modified_date = properties
            .get("modified_date")
            .and_then(|v| v.as_i64())
            .ok_or("No modified date in property")?;
        let (sphere_id, date, _) = crate::utilities::match_spherical_name(&filepath)?;

        let artist: Option<String> = properties
            .get("artist")
            .and_then(|v| v.as_str())
            .and_then(|s| Some(s.to_string()));
        let description = properties
            .get("label")
            .and_then(|v| v.as_str())
            .and_then(|s| Some(s.to_string()))
            .ok_or("No label in property")?;

        let coords = feature
            .get("geometry")
            .and_then(|v| v.get("coordinates"))
            .and_then(|v| v.as_array())
            .ok_or("No geometry in feature")?
            .iter()
            .filter_map(|v| v.as_f64())
            .collect::<Vec<f64>>();

        let longitude = *coords.get(0).ok_or("No longitude in coordinate")?;
        let latitude = *coords.get(1).ok_or("No latitude in coordinate")?;

        Ok(SphericalImage {
            filepath,
            sphere_id,
            date,
            description,
            artist,
            longitude,
            latitude,
            modified_date,
        })

        //let modified_date = chrono::Utc.timestamp_millis().to_rfc2822();
    }
}

pub fn load_metadata(filepath: &PathBuf) -> Map<String, Value> {
    let metadata = Metadata::new_from_path(&filepath).unwrap();

    let mut tags: Vec<String> = Vec::new();

    let tag_results = [
        metadata.get_exif_tags(),
        metadata.get_xmp_tags(),
        metadata.get_iptc_tags(),
    ];

    let mut metadata_hm: Map<String, Value> = Map::new();
    for new_tags in (&tag_results).iter().filter_map(|s| s.as_ref().ok()) {
        for tag in new_tags {
            metadata_hm.insert(
                tag.to_string(),
                metadata.get_tag_multiple_strings(tag).unwrap().into(),
            );
            tags.push(tag.clone())
        }
    }

    metadata_hm
}

/*
pub fn find_spherical_images(dirname: &PathBuf) -> Vec<PathBuf> {

    let mut images = Vec::<PathBuf>::new();
    _find_spherical_images(dirname, &mut images, 0);

    images
}

fn _find_spherical_images(dirname: &PathBuf, images: &mut Vec<PathBuf>, rec_i: usize) {

    if let Ok(readdir) = dirname.read_dir() {

        for filepath in readdir.filter_map(|d| d.ok().and_then(|d2| Some(d2.path()))) {
            if crate::utilities::match_spherical_name(&filepath).is_ok() {
                images.push(filepath);
            } else if filepath.is_dir() & (rec_i < 1000) {
                _find_spherical_images(&filepath, images, rec_i + 1);
            }
        }

    }
}
*/

/*
pub fn index_spheres(dirname: &PathBuf, output_geojson: &PathBuf) -> Result<(), Box<dyn std::error::Error>> {

    let sphere_fps = find_spherical_images(dirname);

    let spheres = sphere_fps.par_iter().map(|filepath| {

        let sphere = SphericalImage::from_path(&filepath).unwrap();

        if !sphere.default_thumbnail_path()?.is_file() {
            sphere.save_thumbnail(&sphere.default_thumbnail_path()?).unwrap();
        };

        sphere

    }).collect::<Vec<SphericalImage>>();



    save_sphericals_to_geojson(&spheres, output_geojson)?;


    Ok(())

}
*/

pub fn sphericals_to_geojson(spheres: &Vec<SphericalImage>) -> Value {
    json!({
        "type": "FeatureCollection",
        "crs": {
            "type": "name",
            "properties": {
                "name": "urn:ogc:def:crs:OGC:1.3:CRS84",
            }
        },
        "features": spheres.iter().map(|s| json!({
            "type": "Feature",
            "properties": {
                "filename": s.filepath,
                "thumbnail": s.default_thumbnail_path(Some(&PathBuf::from("static/"))).ok(),
                "artist": s.artist,
                "label": s.description,
                "date": s.date,
                "modified_date": s.modified_date,

            },
            "geometry": {
                "type": "Point",
                "coordinates": [s.longitude, s.latitude]
            }
        })).collect::<Vec<Value>>()
    })
}

pub fn save_sphericals_to_geojson(
    spheres: &Vec<SphericalImage>,
    filepath: &PathBuf,
    static_dir: Option<&PathBuf>,
) -> Result<(), FetchError> {
    if let Some(parent) = filepath.parent() {
        if !parent.is_dir() {
            std::fs::create_dir(parent)?;
        };
    }

    std::fs::write(
        filepath,
        serde_json::to_string_pretty(&sphericals_to_geojson(spheres))
            .map_err(|e| format!("Error in converting sphericals to JSON: {e:?}"))?,
    )?;

    Ok(())
}
