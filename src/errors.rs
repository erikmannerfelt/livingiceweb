use thiserror::Error;

#[derive(Error, Debug)]
pub enum FetchError {
    DownloadErr(#[from] reqwest::Error),
    WriteErr(#[from] std::io::Error),
    ImageError(#[from] image::error::ImageError),
    FormatErr(String),
}
impl From<String> for FetchError {
    fn from(err: String) -> FetchError {
        FetchError::FormatErr(err)
    }
}

impl From<&str> for FetchError {
    fn from(err: &str) -> FetchError {
        FetchError::from(err.to_string())
    }
}

impl std::fmt::Display for FetchError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&format!("{}", self))
    }
}
