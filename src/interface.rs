use cached::proc_macro::{cached, once};
use rocket::fs::{relative, FileServer};
use rocket::http::ContentType;
use rocket::response::Redirect;
use rocket::State;
use rocket_analytics::Analytics;
use rocket_dyn_templates::Template;
use serde_json::{json, Value};
use std::os::unix::prelude::OsStrExt;
use std::path::PathBuf;
use std::str::FromStr;
use tokio::runtime;

use crate::rephotography::Rephoto;

#[get("/")]
fn index() -> Redirect {
    Redirect::to(uri!(map))
}

#[get("/home")]
fn home_page(state: &State<ServerConfig>) -> Result<Template, String> {
    let home_dir = state.runtime_dir.join("text/home");

    let texts = crate::text::make_text_context(home_dir)
        .map_err(|e| format!("Internal server error: {e:?}"))?;
    let context = json!({ "text": texts });

    Ok(Template::render("home", context))
}

#[get("/projects")]
fn projects_page(state: &State<ServerConfig>) -> Result<Template, String> {
    let projects_dir = state.runtime_dir.join("text/projects");

    let texts = crate::text::make_text_context(projects_dir)
        .map_err(|e| format!("Internal server error: {e:?}"))?;
    let context = json!({ "text": texts });

    Ok(Template::render("projects", context))
}

#[get("/map")]
fn map() -> Template {
    let context = json!({"a": 1});
    Template::render("map", context)
}

#[get("/spheres/<filename>")]
fn sphere(filename: &str) -> Template {
    let context = json!({ "filename": filename });
    Template::render("sphere", context)
}

#[get("/surge_animations/<key>")]
fn surge_animation(key: &str) -> Template {
    let context = json!({ "key": key });
    Template::render("surge_animation", context)
}

#[cached(time = 3600)]
async fn _get_shape(
    filename: String,
    static_dir: PathBuf,
    server_url: String,
) -> Result<(ContentType, Vec<u8>), String> {
    let filepath = static_dir.join("shapes/").join(&filename);

    let content = match filepath.is_file() {
        true => std::fs::read_to_string(&filepath).unwrap(),
        false => {
            let response = match reqwest::get(format!("{server_url}/shapes/{filename}")).await {
                Ok(v) => Ok(v),
                Err(e) => Err(format!("{:?}", e)),
            }?;
            response.text().await.unwrap()
        }
    };

    let content_type = match filename.to_lowercase().ends_with("json") {
        true => ContentType::JSON,
        false => ContentType::Text,
    };

    Ok((content_type, content.into_bytes()))
}

#[get("/shapes/<filename>")]
async fn get_shape(
    filename: String,
    state: &State<ServerConfig>,
) -> Result<(ContentType, Vec<u8>), String> {
    _get_shape(filename, state.static_dir.clone(), state.server_url.clone()).await
}

/*
#[get("/rephotographs.json")]
fn rephotos_json() -> (ContentType, String) {
    let rephotos = crate::rephotography::find_rephotos(
        &std::path::PathBuf::from_str("static/rephotography").unwrap(),
    )
    .unwrap_or(Vec::new());

    (ContentType::JSON, serde_json::to_string(&rephotos).unwrap())
}
*/

//#[once(time = 3600, sync_writes = true)]
#[cached(time = 3600)]
fn get_rephoto_context(rephoto_id: String, static_dir: PathBuf) -> Result<Value, String> {
    let rephoto = Rephoto::from_geojson_file(
        &static_dir.join("shapes/rephotography.geojson"),
        &rephoto_id,
    )
    .map_err(|e| format!("Internal error getting rephotograph: {e}"))?;

    let context = json!({
       "rephoto": rephoto,
       "before_date_str": rephoto.before_date_str(),
       "after_date_str": rephoto.after_date_str()
    });

    Ok(context)
}

#[get("/rephotography/<rephoto_id>")]
fn rephoto(rephoto_id: &str, state: &State<ServerConfig>) -> Result<Template, String> {
    let context = get_rephoto_context(rephoto_id.to_string(), state.static_dir.clone())?;
    Ok(Template::render("rephotography", context))
}

#[get("/static/tiles/esri_satellite/<z>/<x>/<y>")]
#[cached(size = 20000)]
async fn esri_satellite(z: String, x: String, y: String) -> Result<(ContentType, Vec<u8>), String> {
    let response = match reqwest::get(format!("https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{}/{}/{}", z, x, y)).await {
        Ok(v) => v,
        Err(e) => return Err(format!("{:?}", e).into())
    };

    let bytes = match response.bytes().await {
        Ok(b) => b,
        Err(e) => return Err(format!("{:?}", e).into()),
    }
    .iter()
    .map(|v| *v)
    .collect::<Vec<u8>>();

    Ok((ContentType::PNG, bytes))
}

#[cached]
fn _get_spherical_image_icon(
    static_dir: PathBuf,
    color: Option<String>,
) -> Result<Vec<u8>, String> {
    let mut data = match std::fs::read_to_string(static_dir.join("icons/spherical_image.svg")) {
        Ok(d) => d,
        Err(e) => return Err(format!("{:?}", e).into()),
    };

    if let Some(color) = color.and_then(|c| Some(c.to_lowercase())) {
        let okay_chars = "abcdef0123456789".chars().collect::<Vec<char>>();

        if (color.len() != 6) | (color.chars().any(|c| !okay_chars.contains(&c))) {
            return Err(format!("Color {color} not a valid hex string (e.g. 00ff00)").into());
        };
        data = data.replace("style=\"fill:#ffffff", &format!("style=\"fill:#{color}"));
    };

    Ok(data.into_bytes())
}

#[get("/icons/spherical_image.svg?<color>")]
fn spherical_image_icon(
    color: Option<String>,
    state: &State<ServerConfig>,
) -> Result<(ContentType, Vec<u8>), String> {
    Ok((
        ContentType::SVG,
        _get_spherical_image_icon(state.static_dir.clone(), color)?,
    ))
}

#[cached]
fn _get_xyz_tiles(server_url: String, runtime_dir: PathBuf) -> Result<Vec<u8>, String> {
    let mut tiles = Vec::<Value>::new();

    for row in serde_json::from_str::<Value>(
        &std::fs::read_to_string(runtime_dir.join("xyz_tiles.json"))
            .map_err(|e| format!("Internal error reading xyz_tiles.json: {e:?}"))?,
    )
    .unwrap()
    .as_array()
    .unwrap()
    {
        let url = match row.get("url").and_then(|o| o.as_str()) {
            Some(v) => Ok(v),
            None => Err(format!("Could not find url for row {row:?}")),
        }?
        .replace("SERVER_URL", &server_url);

        let bounds: Vec<Vec<f64>> = row
            .get("bounds")
            .and_then(|o| o.as_array())
            .unwrap_or(&vec![])
            .iter()
            .filter_map(|o| {
                o.as_array()
                    .and_then(|a| Some(a.iter().filter_map(|o2| o2.as_f64()).collect::<Vec<f64>>()))
            })
            .collect();

        tiles.push(json!({
            "url": url,
            "title": match row.get("title").and_then(|o| o.as_str()) {
                Some(v) => Ok(v),
                None => Err(format!("Could not find title for row {row:?}"))
            }?,
            "attribution": match row.get("attribution").and_then(|o| o.as_str()) {
                Some(v) => Ok(v),
                None => Err(format!("Could not find attribution for row {row:?}"))
            }?,
            "minZoom": row.get("minZoom").and_then(|o| o.as_i64()).unwrap_or(0_i64),
            "maxZoom": row.get("maxZoom").and_then(|o| o.as_i64()).unwrap_or(18_i64),
            "tileSize": row.get("tileSize").and_then(|o| o.as_i64()).unwrap_or(256_i64),
            "zoomOffset": row.get("zoomOffset").and_then(|o| o.as_i64()).unwrap_or(0_i64),
            "bounds": match (bounds.len() == 2) & bounds.iter().all(|v| v.len() == 2) {
                true => Some(bounds),
                false => None
            },
        }))
    }

    Ok(serde_json::to_vec_pretty(&tiles).unwrap())
}

#[get("/xyz_tiles.json")]
fn xyz_tiles(state: &State<ServerConfig>) -> Result<(ContentType, Vec<u8>), String> {
    Ok((
        ContentType::JSON,
        _get_xyz_tiles(state.server_url.clone(), state.runtime_dir.clone())?,
    ))
}

#[get("/about")]
fn about(state: &State<ServerConfig>) -> Result<Template, String> {
    let about_dir = state.runtime_dir.join("text/about");

    let texts = crate::text::make_text_context(about_dir)
        .map_err(|e| format!("Internal server error: {e:?}"))?;
    let context = json!({ "text": texts });

    Ok(Template::render("about", context))
}

/// Look for a preexisting static directory
/// First it looks by the executable, then it traverses
pub fn get_preexisting_statics() -> PathBuf {
    if let Some(exe_path) = std::env::current_exe()
        .ok()
        .and_then(|p| p.canonicalize().ok())
    {
        let mut directory: Option<PathBuf> = exe_path.parent().and_then(|p| Some(p.to_owned()));

        for _ in 0..3 {
            if let Some(dir_iter) = directory.clone().and_then(|d| d.read_dir().ok()) {
                for subdir in dir_iter.filter_map(|d| d.ok()) {
                    if subdir.file_name() == "static" {
                        return subdir.path();
                    }
                }
                directory = directory.and_then(|d| d.parent().and_then(|p| Some(p.to_owned())))
            }
        }
    };
    let relative = PathBuf::from("static/");

    if !relative.is_dir() {
        panic!("No preexisting static directory found.");
    }

    relative
}

pub fn build_symlinks(static_dir: &PathBuf) {
    let preexisting_statics = get_preexisting_statics();

    for subdir in preexisting_statics
        .read_dir()
        .unwrap()
        .filter_map(|d| d.ok())
    {
        let new_subdir = static_dir.join(subdir.path().file_stem().unwrap());

        if !new_subdir.is_dir() {
            std::fs::create_dir_all(&new_subdir).unwrap();
        };

        for subfile in subdir.path().read_dir().unwrap().filter_map(|d| d.ok()) {
            let new_subfile = new_subdir.clone().join(subfile.path().file_name().unwrap());

            if new_subfile.is_symlink() {
                std::fs::remove_file(&new_subfile).unwrap();
            };
            if new_subfile.is_file() {
                eprintln!("Symlinking to {new_subfile:?} failed. File in the way.");
                continue;
            };

            std::os::unix::fs::symlink(subfile.path().canonicalize().unwrap(), &new_subfile)
                .unwrap();
        }
    }
}

#[derive(Debug)]
pub struct ServerConfig {
    pub static_dir: PathBuf,
    pub server_url: String,
    pub runtime_dir: PathBuf,
}

impl ServerConfig {
    pub fn from_env() -> ServerConfig {
        dotenv::dotenv().ok();

        let static_dir = match std::env::var("STATIC_DIR").ok() {
            Some(s) => PathBuf::from(s),
            None => {
                let home_dir = home::home_dir().unwrap();

                home_dir.join(".cache/livingiceweb/static/")
            }
        };

        let server_url = std::env::var("SERVER_URL")
            .expect("SERVER_URL for the static file server needs to be defined");

        let runtime_dir = get_preexisting_statics()
            .parent()
            .expect("Static dir doesn't have a parent")
            .to_path_buf();
        ServerConfig {
            static_dir,
            server_url,
            runtime_dir,
        }
    }
}

//#[launch]
pub fn build_rocket() -> Result<rocket::Rocket<rocket::Build>, Box<dyn std::error::Error>> {
    //crate::images::index_spheres(&PathBuf::from("static/spheres/"), &PathBuf::from("static/shapes/spheres.geojson"))?;

    let config = ServerConfig::from_env();

    build_symlinks(&config.static_dir);

    let mut rocket_config = rocket::Config::figment();

    // If template_dir is not defined, set it as the runtime directory + "/templates"
    if rocket_config.find_value("template_dir").is_err() {
        rocket_config = rocket_config.merge((
            "template_dir",
            config.runtime_dir.join("templates").to_str().unwrap(),
        ));
    };

    println!("{config:?}");

    let analytics_key = match std::env::var("LIVINGICE_ANALYTICS_KEY") {
        Ok(key) => key,
        Err(_) => {
            eprintln!("\n\nWarning: LIVINGICE_ANALYTICS_KEY not found. Inserting dummy key.\n\n");
            "DUMMY".to_string()
        }
    };

    Ok(rocket::custom(rocket_config)
        .mount(
            "/",
            routes![
                home_page,
                projects_page,
                index,
                map,
                sphere,
                surge_animation,
                esri_satellite,
                rephoto,
                //rephotos_json,
                spherical_image_icon,
                xyz_tiles,
                get_shape,
                about
            ],
        )
        .mount("/static", FileServer::from(config.static_dir.clone()))
        .attach(Template::fairing())
        .attach(Analytics::new(analytics_key))
        .manage(config))
}
