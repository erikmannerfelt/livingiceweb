use serde_json::{Map, Value};
use std::path::PathBuf;

fn metadata_str_as_coordinate(coord_str: &str, ref_str: &str) -> Option<f64> {
    let mut val = 0_f64;
    for (j, part) in coord_str.split(" ").enumerate() {
        if let Some(p) = part
            .splitn(2, "/")
            .map(|s| s.parse::<f64>().ok())
            .collect::<Option<Vec<f64>>>()
        {
            val += (p[0] / p[1]) / 60_f64.powi(j as i32);
        } else {
            return None;
        };
    }
    if ["S", "W"].iter().any(|l| ref_str.contains(l)) {
        val *= -1.;
    };
    Some(val)
}

pub fn coordinates_from_metadata(metadata: &Map<String, Value>) -> Option<(f64, f64, Option<f64>)> {
    let tags = [
        "Exif.GPSInfo.GPSLongitude",
        "Exif.GPSInfo.GPSLongitudeRef",
        "Exif.GPSInfo.GPSLatitude",
        "Exif.GPSInfo.GPSLatitudeRef",
        "Exif.GPSInfo.GPSAltitude",
    ];

    let parsed = tags
        .iter()
        .map(|tag| {
            metadata
                .get(&tag.to_string())
                .and_then(|array| array.get(0))
                .and_then(|json_string| json_string.as_str())
                .and_then(|borrowed| Some(borrowed.to_string()))
                .unwrap_or("".to_string())
        })
        .collect::<Vec<String>>();

    let mut lon_lat = Vec::<f64>::new();
    for i in [0, 2] {
        match metadata_str_as_coordinate(&parsed[i], &parsed[i + 1]) {
            Some(v) => lon_lat.push(v),
            None => return None,
        }
    }

    // Sometimes the coordinate is 0.0000 degrees. That is obviously wrong
    if lon_lat.iter().any(|v| v == &0.) {
        return None;
    };

    if let Some(alt_parts) = parsed[4]
        .splitn(2, "/")
        .map(|s| s.parse::<f64>().ok())
        .collect::<Option<Vec<f64>>>()
    {
        let alt = alt_parts[0] / alt_parts[1];
        Some((lon_lat[0], lon_lat[1], Some(alt)))
    } else {
        Some((lon_lat[0], lon_lat[1], None))
    }
}

pub fn supported_formats() -> Vec<String> {
    let formats = vec!["jpg", "jpeg", "png", "tif", "tiff"];

    let mut formats = formats
        .iter()
        .map(|s| s.to_string())
        .collect::<Vec<String>>();

    formats.append(
        &mut formats
            .iter()
            .map(|s| s.to_uppercase())
            .collect::<Vec<String>>(),
    );

    formats
}

fn format_survey_name(name: &str) -> String {
    let mut name = name.replace("-", " ").replace("_", " ").trim().to_string();

    let name_lower = name.to_lowercase();

    // "vert", "cvert", and "obl" were annotations for the type of survey pre-2019
    // Vertical, Combined-oblique-and-Vertical, Oblique
    // These are not needed anymore and can be removed for niceness
    for ending in [" cvert", " vert", " obl"] {
        if name_lower.ends_with(ending) {
            for _ in 0..ending.chars().count() {
                name.pop();
            }
        };
    }

    let mut new_chars = Vec::<char>::new();
    let mut prev = ' ';
    let name_chars = name.chars().collect::<Vec<char>>();
    let n = name_chars.len();
    let mut i = 0_usize;

    while i < n {
        let next_char = name_chars.get(i + 1);
        let c = name_chars[i];

        if i == 0 {
            new_chars.push(c);
        } else if c.is_uppercase() & prev.is_uppercase() {
            if next_char
                .and_then(|n| Some(n.is_lowercase()))
                .unwrap_or(false)
            {
                new_chars.push(' ');
                new_chars.push(c)
            } else {
                new_chars.push(c);
            }
        } else if c.is_uppercase() & !prev.is_uppercase() {
            new_chars.push(' ');
            new_chars.push(c);
        } else {
            new_chars.push(c);
        };
        prev = c;
        i += 1;
    }
    let mut new_name = new_chars.iter().collect::<String>();

    // Remove all multiple whitespaces
    if let Ok(re) = regex::Regex::new(r"\s\s+") {
        new_name = re.replace_all(&new_name, " ").to_string();
    };

    new_name
}

pub fn parse_date_str(date: &str) -> Result<String, String> {
    let date_chars = date.chars().collect::<Vec<char>>();

    if date_chars.iter().all(|c| c.is_numeric()) {
        let expected_format = match date_chars.len() {
            6 => Ok("%y%m%d"),
            8 => Ok("%Y%m%d"),
            n => Err(format!(
                "All-numeric date '{date}' of length {n} is not supported"
            )),
        }?;
        match chrono::NaiveDate::parse_from_str(date, expected_format) {
            Ok(dt) => Ok(dt.format("%Y-%m-%d").to_string()),
            Err(e) => Err(format!("{e:?}")),
        }
    } else {
        Err(format!("Could not parse date: '{date}'"))
    }
}

pub fn match_spherical_name(filename: &PathBuf) -> Result<(String, String, String), String> {
    if let Some(ext) = filename
        .extension()
        .and_then(|osstr| osstr.to_str().and_then(|s| Some(s.to_string())))
    {
        if !supported_formats().contains(&ext) {
            Err("Format unsupported".into())
        } else {
            if let Some(name) = filename.file_stem().and_then(|osstr| osstr.to_str()) {
                let parts = name.splitn(3, "_").collect::<Vec<&str>>();

                if parts.len() < 3 {
                    Err("TODO".into())
                } else {
                    let date = parse_date_str(&parts[1].to_string())?;

                    let description = format_survey_name(
                        parts[2]
                            .split_once("-")
                            .and_then(|(_, des)| Some(des))
                            .unwrap_or(""),
                    );

                    let sphere_id =
                        "Sph_".to_string() + parts[1] + "_" + &parts[2].split("-").next().unwrap();
                    Ok((sphere_id, date, description))
                }
            } else {
                Err("No valid file stem of the filepath".into())
            }
        }
    } else {
        Err("File has no extension".into())
    }
}

pub fn filepath_is_url(filepath: &PathBuf) -> bool {
    filepath.as_os_str().to_str().unwrap().contains("://")
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_metadata_str_as_coordinate() {
        use super::metadata_str_as_coordinate;

        let cases: Vec<((&str, &str), Option<f64>)> = vec![
            (("78/1 29/1 600000/10000", "N"), Some(78.5)),
            (("78/1 33/1 129506/10000", "N"), Some(78.55359738888889)),
            (("12/1 51/1 564729/10000", "E"), Some(12.865686916666666)),
            (("600/10 30/1 0/1", "S"), Some(-60.5)),
            (("600/10 0/1 0/1", "W"), Some(-60.)),
            (("635/100", ""), Some(6.35)),
            (("656///", ""), None),
        ];

        for (case_test, case_expected) in cases {
            assert_eq!(
                metadata_str_as_coordinate(case_test.0, case_test.1),
                case_expected
            );
        }
    }

    #[test]
    fn test_format_survey_name() {
        use super::format_survey_name;

        assert_eq!(format_survey_name("Survey1"), "Survey1");

        assert_eq!(format_survey_name("CamelCaseSurvey"), "Camel Case Survey");

        assert_eq!(
            format_survey_name("ABBREVIATIONThenCamelCase"),
            "ABBREVIATION Then Camel Case"
        );

        assert_eq!(
            format_survey_name("MixOf Camel Case and normal"),
            "Mix Of Camel Case and normal"
        );

        assert_eq!(format_survey_name("A Survey Vert"), "A Survey");
        assert_eq!(format_survey_name("A Survey CVert"), "A Survey");
        assert_eq!(format_survey_name("A Survey Obl"), "A Survey");
        assert_eq!(
            format_survey_name("ASurveyWithVertInTheMiddle"),
            "A Survey With Vert In The Middle"
        );
    }

    #[test]
    fn test_parse_date_str() {
        use super::parse_date_str;

        let cases: Vec<(&str, Result<&str, &str>)> = vec![
            ("220802", Ok("2022-08-02")),
            ("20220802", Ok("2022-08-02")),
            ("19950702", Ok("1995-07-02")),
        ];

        for (case_test, case_expected) in cases {
            let case_expected = match case_expected {
                Ok(s) => Ok(s.to_string()),
                Err(e) => Err(e.to_string()),
            };
            assert_eq!(parse_date_str(case_test), case_expected);
        }
    }

    #[test]
    fn test_match_spherical_name() {
        use super::match_spherical_name;
        use std::path::PathBuf;
        use std::str::FromStr;

        let cases: Vec<(&str, Result<(&str, &str, &str), &str>)> = vec![
            (
                "./Sph_220802_A-Testing.jpg",
                Ok(("Sph_220802_A", "2022-08-02", "Testing")),
            ),
            (
                "./Sph_220822_A-Testing.JPEG",
                Ok(("Sph_220822_A", "2022-08-22", "Testing")),
            ),
            (
                "/home/user/Sph_19950705_A-Hello.tif",
                Ok(("Sph_19950705_A", "1995-07-05", "Hello")),
            ),
            (
                "./Sph_160822_I-TestingLongName.jpg",
                Ok(("Sph_160822_I", "2016-08-22", "Testing Long Name")),
            ),
            (
                "./Sph_220802_AA-Testing.jpg",
                Ok(("Sph_220802_AA", "2022-08-02", "Testing")),
            ),
        ];

        for (case_test, case_expected) in cases {
            let case_expected = match case_expected {
                Ok((a, b, c)) => Ok((a.to_string(), b.to_string(), c.to_string())),
                Err(e) => Err(e.to_string()),
            };
            assert_eq!(
                match_spherical_name(&PathBuf::from_str(case_test).unwrap()),
                case_expected
            );
        }
    }
}
