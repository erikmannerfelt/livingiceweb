## Running
See the `run.sh` script.
For releases, run `./run.sh --release`.

### Updating the spherical image database
Either restart the webserver, or run `cargo run --bin spheres`.