#!/usr/bin/env bash
set -e
# helloo

other=(
	rephotography.zip

)

shapes=(
	CryoClim_GAO_SJ_1990.geojson
)
tiles=(
	basal_hillshade_svift_ibcao.zip
	S0_DTM20_hillshade_multi.zip
	dh_1936_2010_all_Svalbard_50m_rendered.zip
	ddem_1936_1990_rendered.zip
	ddem_1990_2010_rendered.zip
	ddem_1990_2018_rendered.zip
	ddem_2010_2018_rendered.zip
	ddem_squared_1936_1990_2010_2018_rendered.zip
	ortho_1936_all_Svalbard_20m.zip
)


mkdir -p static/shapes/ 2> /dev/null
n=1
n_files=`echo "${shapes[@]}" | wc -w`

for filename in "${shapes[@]}"; do
	echo "Downloading shape $n/$n_files: $filename"
	curl -so "static/shapes/$filename" "https://schyttholmlund.com/share/GlacioSvalbard/shapes/$filename"
	n=$(($n + 1))
done


mkdir static/tiles/ -p 2> /dev/null
n=1
n_files=`echo "${tiles[@]}" | wc -w`

for filename in "${tiles[@]}"; do
	echo "Downloading tile $n/$n_files: $filename"

	curl -so temp.zip "https://schyttholmlund.com/share/GlacioSvalbard/tiles/$filename"
	unzip -q temp.zip -d static/tiles/

	rm temp.zip
	n=$(($n + 1))
done

n=1
n_files=`echo "${other[@]}" | wc -w`

for filename in "${other[@]}"; do
	echo "Downloading other data $n/$n_files: $filename"

	curl -so temp.zip "https://schyttholmlund.com/share/GlacioSvalbard/$filename"
	unzip -q temp.zip -d static/

	rm temp.zip
	n=$(($n + 1))
done
