# syntax=docker/dockerfile:1
FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive

# Install python and build tools
RUN apt -y update
RUN apt install python3 python3-pip build-essential curl libssl-dev pkg-config gdal-bin unzip libglib2.0-dev libgexiv2-dev -y

WORKDIR /code

# Install nodejs and npm using nvm to get the latest version.                               
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash         

# TODO: Make a better function sourcing approach than this.                                 
# The nvm command is a function defined in nvm.sh, so it needs to be sourced like this.     
# I don't understand how to source it once in docker, so the "\. blabla" has to be prepended   to each command.                                                                           
RUN \. "$HOME/.nvm/nvm.sh" && nvm install node && nvm install-latest-npm
RUN mkdir static/plugins -p
RUN \. "$HOME/.nvm/nvm.sh" && npm install --prefix ./static/plugins photo-sphere-viewer leaflet leaflet.markercluster juxtaposejs

# Install rust
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN rustup install stable

WORKDIR /code

#ADD ./download_data.sh /code/
#RUN bash ./download_data.sh

# Add the rest of the 
ADD ./Cargo.lock /code/
ADD ./Cargo.toml /code/
ADD ./Rocket.toml /code/
ADD ./templates /code/templates/
ADD ./src /code/src/

RUN cargo build --release

ADD ./static/js/ /code/static/js/
ADD ./static/icons/ /code/static/icons/

#RUN ls static/rephotography && rm -r static/rephotography
#RUN curl -so temp.zip https://schyttholmlund.com/share/GlacioSvalbard/rephotography.zip && unzip -qo temp.zip -d static/ && rm temp.zip && echo tru
RUN mkdir -p ./static/shapes/
ENV SERVER_URL="https://schyttholmlund.com/share/GlacioSvalbard/static/"
ADD ./xyz_tiles.json /code/
#RUN pip install geopandas Pillow exif


#ADD ./compile_spheres.py /code/
#RUN python3 compile_spheres.py

CMD ["cargo", "run", "--release"]
